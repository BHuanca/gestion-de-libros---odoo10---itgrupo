# -*- coding: utf-8 -*-
from . import Author
from . import Genre
from . import Book
from . import RentBook