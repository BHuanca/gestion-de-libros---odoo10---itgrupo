# -*- coding: utf-8 -*-
from odoo import models, fields, api

class RentBook(models.Model):
    _name = "book.rent_book"

    user_id = fields.Many2one(string="Usuario para rentar",
        comodel_name="res.partner")
    book_id = fields.Many2one(string="Libros a rentar",
        comodel_name="book.book")
    observation = fields.Text(string="Observacion del libro")
    return_date = fields.Date(string="Fecha de devolucion", required=True)
    loan_date = fields.Date(string="Fecha de prestamo", required=True)

    days_rent = fields.Char(string="Dias rentados")

    #TODO falta completar el campo dias de prestamo y estado del libro
