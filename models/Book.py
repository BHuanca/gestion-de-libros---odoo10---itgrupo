# -*- coding: utf-8 -*-
from odoo import models, fields, api

class Book(models.Model):
    _name = "book.book"
    _description = "Libros para rentar"
    
    author_id = fields.Many2many(comodel_name="book.author",
        string="Autores de los libros")
    name = fields.Char(string="Nombre del Libro", required=True)
    genre_id = fields.Many2many(string="Genero del libro", 
        comodel_name="book.genre", required=True)
    cover = fields.Binary(string="Portada del libro", required=True)
    publication_date = fields.Date(string="Fecha de publicacion", required=True)
    state = fields.Selection([('D','Disponible'),('R','Rentado')], string="Estado del libro")

    #Campo para el boton archivar
    active = fields.Boolean(string="Libro archivado o no")

