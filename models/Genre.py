# -*- coding: utf-8 -*-
from odoo import models, fields, api

class Genre(models.Model):
    _name = "book.genre"
    _description = "Genero de los libros"

    name = fields.Char(string="Genero del libro", required = True)