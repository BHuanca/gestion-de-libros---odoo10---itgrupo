# -*- coding: utf-8 -*-
from odoo import models, fields, api

class Author(models.Model):
    _name = "book.author"
    _description = "Autores de los Libros"

    name = fields.Char(string="Nombre del Autor", required = True)
    