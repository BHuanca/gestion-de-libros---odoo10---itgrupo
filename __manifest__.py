# -*- coding: utf-8 -*-
{
    'name': "Gestion de Libros",
    'summary': """Se podra gestionar los libros que van viniendo y asi facilitar
        el trabajo de los usuarios""",
    'description': 'Modulo para la gestion de libros',
    'author': "Bladimir Huanca Huarachi",
    'category': 'Gestion',
    'version': '0.1',
    'depends': ['base'],
    'data': [
        'views/author_view.xml',
        'views/genre_view.xml',
        'views/book_view.xml',
        'views/rent_book_view.xml'
    ]
}
